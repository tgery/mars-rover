package nasa

import collection.immutable.HashMap


case class Rover(x: Int, y: Int, direction: Char) {

  require(Rover.turnLeft.keys.toList.contains(direction), s"Direction must be in ${Rover.turnLeft.keys.toList}")

  def move(): Rover = direction match {
    case 'N' => Rover(x, y + 1, direction)
    case 'S' => Rover(x, y - 1, direction)
    case 'E' => Rover(x + 1, y, direction)
    case 'W' => Rover(x - 1, y, direction)
  }

  def turnRight(): Rover = Rover(x , y, Rover.turnRight(direction))
  def turnLeft(): Rover = Rover(x , y, Rover.turnLeft(direction))

  override def toString = s"$x $y $direction"
}

object Rover {
  val turnRight = HashMap[Char, Char](
  'N' -> 'E',
  'S' -> 'W',
  'E' -> 'S',
  'W' -> 'N'
  )

  val turnLeft = HashMap[Char, Char](
  'N' -> 'W',
  'S' -> 'E',
  'E' -> 'N',
  'W' -> 'S'
  )
}
