package nasa

import java.io.PrintWriter

import scala.io.{BufferedSource, Source}
import scala.util.{Failure, Success, Try}

object MarsRover {
  def main(args: Array[String]): Unit = {
    if (checkArgs(args)) {
      System.err.println("The first argument must be the path to the file")
      System.exit(1)
    }
    val inputPath = args(0)
    val buffuredSource = Source.fromFile(inputPath)
    try {
      val output = calculateRoversPosition(buffuredSource)
      new PrintWriter("output.txt") {
        write(output)
        close()
      }
      println("Results written in output.txt")
    } catch {
      case e: Throwable =>
        val msg = e.getMessage
        System.err.println(s"Error: $msg")
        System.exit(2)
    } finally {
      buffuredSource.close()
    }
  }

  def getInitPositionOfRovers(lines: List[String]) = lines
    .zipWithIndex.filter { item => {
    val (_, index) = item
    index % 2 == 0
  }
  }.map(_._1)

  def getRoverMovements(lines: List[String]) = lines
    .zipWithIndex.filter { item => {
    val (_, index) = item
    index % 2 != 0
  }
  }.map(_._1)


  def calculateRoversPosition(buffuredSource: BufferedSource): String = {
    val lines = buffuredSource.getLines()
    val craterSizeRaw = lines.next()
    val (maxX, maxY) = parseCraterSize(craterSizeRaw)
    val linesList = lines.toList
    val roverInitPositions: List[String] = getInitPositionOfRovers(linesList)
    val roverMovements: List[String] = getRoverMovements(linesList)
    val craterWithInitialPosition = roverInitPositions.foldLeft(Crater(List(), maxX, maxY)) {
      (crater, roverPositionRaw) => crater.addNewRover(parseInitialState(roverPositionRaw))
    }
    val craterWithAllMovement = roverMovements.zipWithIndex.foldLeft(craterWithInitialPosition) {
      (crater, item) => {
        val (roverInstructionsRaw, index): (String, Int) = item
        crater.applyMoves(index, roverInstructionsRaw)
      }
    }
    craterWithAllMovement.rovers.map({
      case Success(rover) => rover.toString
      case Failure(ex) => ex.getMessage
    }).mkString(sys.props("line.separator"))
  }

  def parseCraterSize(craterSizeRaw: String): (Int, Int) = {
    val splitedInput = craterSizeRaw.trim.split(' ')
    val maxX = splitedInput(0).trim.toInt
    val maxY = splitedInput(1).trim.toInt
    (maxX, maxY)
  }

  def parseInitialState(roverPositionRaw: String): Try[Rover] = {
    val splitedInput = roverPositionRaw.trim.split(' ')
    val xTry = toInt(splitedInput(0))
    val yTry = toInt(splitedInput(1))
    (xTry, yTry) match {
      case (Failure(ex), _) => Failure(new Exception(s"Impossible to parse the x value, must be an integer ${splitedInput(0)}", ex))
      case (_, Failure(ex)) => Failure(new Exception(s"Impossible to parse the y value, must be an integer ${splitedInput(1)}", ex))
      case (Success(x), Success(y)) => Try(Rover(x, y, splitedInput(2).toList.head))
    }

  }

  private def toInt(s: String) = Try(s.trim.toInt)

  private def checkArgs(args: Array[String]) = {
    args.length != 1
  }


}
