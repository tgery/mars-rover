package nasa

import scala.util.{Failure, Success, Try}

case class Crater(rovers: List[Try[Rover]], maxX: Int, maxY: Int) {

  def addNewRover(rover: Try[Rover]): Crater = {
    Crater(rovers ++ List(rover.flatMap(r => verifyRoverPosition(r, -1))), maxX, maxY)
  }

  def applyMoves(roverIdx: Int, moves: String): Crater = {
    val rover: Try[Rover] = moves
      .toList
      .foldLeft(rovers(roverIdx)) {
        (rover, operation) => rover.flatMap(r => this.applyMove(roverIdx, r, operation))
      }
    Crater(rovers.updated(roverIdx, rover), maxX, maxY)
  }

  def applyMove(roverIdx: Int, rover: Rover, action: Char): Try[Rover] = action match {
    case 'M' =>
      val newPosition = rover.move()
      verifyRoverPosition(newPosition, roverIdx)
    case 'R' => Success(rover.turnRight())
    case 'L' => Success(rover.turnLeft())
    case command => Failure(new Exception(s"Unknown command: $command"))
  }



  private def verifyRoverPosition(newPosition: Rover, roverIdx: Int): Try[Rover] = newPosition match {
    case Rover(x, y, _) if x < 0 => Failure(new Exception("The rover crashed on the crater wall as x < 0"))
    case Rover(x, y, _) if y < 0 => Failure(new Exception("The rover crashed on the crater wall as y < 0"))
    case Rover(x, y, _) if x > maxX => Failure(new Exception(s"The rover crashed on the crater wall as x > $maxX"))
    case Rover(x, y, _) if y > maxY => Failure(new Exception(s"The rover crashed on the crater wall as y > $maxY"))
    case Rover(x, y, _) if isRoverCrashingOnOthers(roverIdx, x, y) => Failure(new Exception(s"The rover crashed on another one at ($x,$y)"))
    case _ => Success(newPosition)
  }

  def isRoverCrashingOnOthers(roverIdx: Int, x: Int, y: Int) = {
    val r = rovers
      .zipWithIndex
      .filter(item => {
        val (_, index) = item
        index != roverIdx // We need to exclude the current rover because we might come back to its previous location which is not a blocking case, as he already left it
      }).map(d => d._1)
      .filter(i => i.isSuccess)
      .map(i => i.get).count(r => r.x == x && r.y == y) > 0

    r
  }

}
