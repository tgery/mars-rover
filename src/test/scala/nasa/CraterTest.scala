package nasa

import org.specs2.mutable.Specification

import scala.util.Try


class CraterTest extends Specification {
  "The crater" >> {
    "can move its rover" >> {
      // given
      val rover1 = Rover(1, 2, 'N')
      val rover2 = Rover(4, 2, 'N')
      val moves = "LMLMLMLMM"
      val initialCrater = Crater(List(Try(rover1), Try(rover2)), 5, 5)

      // when
      val crater = initialCrater.applyMoves(0, moves)


      // then
      crater.rovers.head must_== Try(Rover(1, 3, 'N'))
    }

    "detect if a rover leaves the crater" >> {
      // given
      val rover1 = Rover(1, 1, 'S')
      val rover2 = Rover(4, 2, 'N')
      val moves = "MMMLMRM"
      val initialCrater = Crater(List(Try(rover1), Try(rover2)), 5, 5)

      // when
      val crater = initialCrater.applyMoves(0, moves)


      // then
      crater.rovers.head.isFailure must_== true
      crater.rovers.head.failed.get.getMessage must_== "The rover crashed on the crater wall as y < 0"
    }


    "detect if two rovers crash onto each other" >> {
      // given
      val rover1 = Rover(1, 1, 'E')
      val rover2 = Rover(2, 1, 'W')
      val moves = "M"
      val initialCrater = Crater(List(Try(rover1), Try(rover2)), 5, 5)

      // when
      val crater = initialCrater.applyMoves(0, moves)


      // then
      crater.rovers.head.isFailure must_== true
      crater.rovers.head.failed.get.getMessage must_== "The rover crashed on another one at (2,1)"
    }
  }
}
