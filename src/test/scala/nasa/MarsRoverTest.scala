package nasa

import org.specs2.mutable.Specification

import scala.io.Source
import scala.util.Success

class MarsRoverTest extends Specification {
  "calculateRoverPosition" >> {
    "can calculate position from the sample file" >> {
      // given
      val inputStream = Source.fromInputStream(getClass.getResourceAsStream("/sample/input.txt"))

      // when
      val output = MarsRover.calculateRoversPosition(inputStream)

      // then
      val expectedOutput = Source.fromInputStream(getClass.getResourceAsStream("/sample/output.txt")).mkString
      output must_== expectedOutput
    }

    "show errors" >> {
      // given
      val inputStream = Source.fromInputStream(getClass.getResourceAsStream("/going-out-of-the-map/input.txt"))

      // when
      val output = MarsRover.calculateRoversPosition(inputStream)

      // then
      val expectedOutput = Source.fromInputStream(getClass.getResourceAsStream("/going-out-of-the-map/output.txt")).mkString
      output must_== expectedOutput
    }
  }

  "parseInitialState" >> {
    "can parse the init position correctly" >> {
      // given
      val input = "0 0 N"

      // when
      val rover = MarsRover.parseInitialState(input)

      // then
      rover must_== Success(Rover(0, 0, 'N'))
    }

    "can parse the direction" >> {
      // given
      val input = "0 0 S"

      // when
      val rover = MarsRover.parseInitialState(input)

      // then
      rover must_== Success(Rover(0, 0, 'S'))
    }


    "can parse the x parameter" >> {
      // given
      val input = "1 0 S"

      // when
      val rover = MarsRover.parseInitialState(input)

      // then
      rover must_== Success(Rover(1, 0, 'S'))
    }


    "can parse the y parameter" >> {
      // given
      val input = "0 1 S"

      // when
      val rover = MarsRover.parseInitialState(input)

      // then
      rover must_== Success(Rover(0, 1, 'S'))
    }


    "return a failure if x not an integer" >> {
      // given
      val input = "s 1 S"

      // when
      val rover = MarsRover.parseInitialState(input)

      // then
      rover.isFailure must_== true
      rover.failed.get.getMessage must_== "Impossible to parse the x value, must be an integer s"
    }


    "return a failure if y not an integer" >> {
      // given
      val input = "0 t S"

      // when
      val rover = MarsRover.parseInitialState(input)

      // then
      rover.isFailure must_== true
      rover.failed.get.getMessage must_== "Impossible to parse the y value, must be an integer t"
    }


    "return a failure if the direction is not valid" >> {
      // given
      val input = "0 0 U"

      // when
      val rover = MarsRover.parseInitialState(input)

      // then
      rover.isFailure must_== true
      rover.failed.get.getMessage must_== "requirement failed: Direction must be in List(E, N, W, S)"
    }

  }
  "parseCraterSize" >> {

    "return the value parsed" >> {
      // given
      val input = "4 7"

      // when
      val (x, y) = MarsRover.parseCraterSize(input)

      // then
      x must_== 4
      y must_== 7
    }
  }
}
