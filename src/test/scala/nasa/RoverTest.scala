package nasa

import org.specs2.mutable.Specification


class RoverTest extends Specification {
  "The rover" >> {
    "can move on the left" >> {
      // given
      val initialPosition = Rover(1, 2, 'N')

      // when
      val rover = initialPosition
        .turnLeft()
        .move()
        .turnLeft()
        .move()
        .turnLeft()
        .move()
        .turnLeft()
        .move()
        .move()

      // then
      rover must_== Rover(1, 3, 'N')
    }

    "can move on the right" >> {
      // given
      val initialPosition = Rover(3, 3, 'E')

      // when
      val rover = initialPosition
        .move()
        .move()
        .turnRight()
        .move()
        .move()
        .turnRight()
        .move()
        .turnRight()
        .turnRight()
        .move()

      // then
      rover must_== Rover(5, 1, 'E')
    }
  }
}
